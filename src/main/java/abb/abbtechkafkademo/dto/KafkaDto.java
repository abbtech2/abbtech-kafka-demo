package abb.abbtechkafkademo.dto;

import lombok.Data;

@Data
public class KafkaDto {

    private int id;
    private String name;
}
