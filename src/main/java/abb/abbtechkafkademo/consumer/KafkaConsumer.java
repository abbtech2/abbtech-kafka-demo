package abb.abbtechkafkademo.consumer;

import abb.abbtechkafkademo.dto.KafkaDto;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

    @KafkaListener(topics = "my_first_topic_1", groupId = "kafka-group")
    public void listerKafkaDto(KafkaDto kafkaDto) {
        System.out.println("Received message from Consumer 1: " + kafkaDto);
    }

}
